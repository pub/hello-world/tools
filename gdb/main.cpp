#include <iostream>
#include <cmath>

// This is a simple function that checks if numbers are prime. Or does it? There may be a bug...
bool is_prime(int x) {
    for (int i = 2; i < std::sqrt(x); ++i) {
        if (x % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    for (int i = 10; i < 30; ++i) {
        bool const prime = is_prime(i);
        if (prime) {
            std::cout << i << " is prime\n";
        } else {
            std::cout << i << " is not prime\n";
        }
    }

    return 0;
}
