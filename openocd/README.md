# OpenOCD

[OpenOCD](http://openocd.org/) is a tool for on-chip debugging. It's also a great tool just for
flashing programs and bootloaders.

## Installation

### Package Managers

On Linux, TODO.

On Mac, using [Homebrew](https://brew.sh/): `brew install openocd`. As of Dec. 5, 2019, this gives
you version 0.10.0, which supports ATSAMD11, but not SAMD51. So you if you want to use the latter
chip, you might need to build from source.

On Windows, there is no package manager. But you can grab binaries
[here](https://gnutoolchains.com/arm-eabi/openocd/). Once you've downloaded the latest release,
unpack it and put it wherever you want it to live. The important file is `bin/openocd`. You can
either type out the full path whenever you want to run it (e.g. something like `C:\\Program
Files\openocd\bin\openocd`, instead of just `openocd`), or you can add the `bin` dir to your path.
(See
[here](https://www.howtogeek.com/118594/how-to-edit-your-system-path-for-easy-command-line-access/)
or Google elsewhere for instructions on adding directories to your path.)

### From Source

*This section is for Linux and Mac only*

If you want to know exactly which version you're using, or you're going for maximum bare metal cred,
you can build from source. This can be helpful if you'll be developing between different operating
systems &mdash; one less thing that could be different &mdash; or if the package available on your
system is out of date.

The official [git repo](https://sourceforge.net/p/openocd/code/ci/master/tree/)
is on SourceForge.

```git clone https://git.code.sf.net/p/openocd/code openocd-code```

To build it, you'll need `gcc`, as well as `make`, `libtool`, `pkg-config` version 0.23
or greater, and possibly others detailed [here](http://openocd.org/doc-release/README). Once you
have the prereqs, building is a standard process. Just note that you need to enable cmsis-dap,
which is the protocol your computer will use to communicate with an ATMEL ICE debugger.

```
./bootstrap
./configure --enable-cmsis-dap
make
make install
```

Compiler note: you should be able to use `clang`, but it doesn't seem to work (at least on Mac).
I get the following error:

```
src/flash/nor/nrf5.c:613:5: error: format specifies type 'unsigned short' but the argument has type 'uint32_t' (aka 'unsigned int') [-Werror,-Wformat]
```
To fix it, tell the configuration script to use gcc: `/configure --enable-cmsis-dap CC=gcc-8` (or whatever gcc you have installed).
Note that the default `gcc` command on Mac is not actually `gcc`; it's `clang`. So you'll have to install it if you haven't already.

### Additional Setup

You'll almost certainly end up needing a handful of other helpful tools. Depending on your other
work you may already have many of these on your machine.

*This command assumes Linux*

```
sudo apt install autoconf build-essential cmake libtool libtool-bin libhidapi-dev libusb-dev libusb-1.0-0-dev pkg-config
```

Another essential one is [`gdb`](https://www.gnu.org/software/gdb/) (if you want to do any
debugging). Most documents on the web say to install `gdb-arm-none-eabi`, but unless you're using an
older version of `gdb` these days you're going to want `gdb-multiarch`.

```
sudo apt install gdb-multiarch
```

## Basic Use

### Config files

When OpenOCD starts, it first processes some configuration commands. You can specify a specific
config file with `openocd -f path/to/your/config.cfg`, or by default it looks for `openocd.cfg`. At a minimum, OpenOCD needs to know how to communicate with the
programming/debug tool you're using (e.g. ATMEL ICE), and what microcontroller you're using. For
ATMEL ICE, we want `cmsis-dap`. (You can read more about what this means
[here](https://electronics.stackexchange.com/questions/405134/swdap-vs-cmsis-dap-vs-daplink).)
