# Tools

This repo is intended to document software tools that are used across multiple microcontrollers.

Underway:

- [OpenOCD](openocd)

Planned:

- Arduino (with support for boards we've flashed)
- PlatformIO
- gdb

Improved Documentation:

this repo is referenced in and extended upon in [the Practical Microcontroller Primer](https://mtm.cba.mit.edu/2021/2021-10_microcontroller-primer/) over on MTM. Check it out!
