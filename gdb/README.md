# GDB

[GDB](https://www.gnu.org/software/gdb/) is a program that looks inside other programs. It's a
really handy tool for tracking down hard to find bugs.

## Setup

First, GDB is only useful if you have a way to compile programs (usually C or C++, but GDB does
support a variety of other languages as well). So to use it on your computer, you'll probably want
to get set up with [gcc and make](https://gitlab.cba.mit.edu/pub/hello-world/c_cpp_and_make). For
debugging microcontrollers, you'll need a compiler that supports your particular chip (`gcc-avr`,
`arm-none-eabi`, etc.).

To debug your microcontroller, you'll need some on-chip debugging software.
[OpenOCD](https://gitlab.cba.mit.edu/pub/hello-world/tools/-/tree/master/openocd) is the option I'm
familiar with.

To install GDB itself, on Linux or Mac you can probably use your system's package manager (`apt` or
`yum` on linux, [`homebrew`](https://brew.sh/) on Mac). If you want complete hacker cred, you could
build it from [source](https://www.gnu.org/software/gdb/current/).

## Debugging on Your Computer

Let's try running the example program in this directory. It should tell us which numbers are prime.

```
10 is not prime
11 is prime
12 is not prime
13 is prime
14 is not prime
15 is not prime
16 is not prime
17 is prime
18 is not prime
19 is prime
20 is not prime
21 is not prime
22 is not prime
23 is prime
24 is not prime
25 is prime
26 is not prime
27 is not prime
28 is not prime
29 is prime
```

But wait, what's going on with 25? We've got a bug.

To find it, run `gdb main`. This launches `gdb`.

Before we do anything else, let's set a breakpoint at line 16: `break main.cpp:16`. Now type `run`.

Your program should be paused at line 16. Try `print i`. Then try `continue`, and `print i` again.

Other helpful commands include:

- `list` shows the line of code we're at, and a few before and after
- `print <variable>` shows value of a variable
- `break <fileanme:line>` adds a breakpoint
- `info breakpoints` prints the existing breakpoints
- `continue` continues until the next break point, exception, etc.
- `next` steps forward one line, but doesn't step into functions
- `step` steps forward one line, and does go inside functions
- `bt` or `backtrace` shows the current thread

See if you can use these to find the bug.

## Debugging on Your Microcontroller

On a microcontroller, it's much the same. Except we need some extra hardware and software to talk to
our micrcontroller for us. I'll use Atmel ICE and OpenOCD.

For ARM microcontrollers, first locate the `.elf` file that you built. Connect Atmel ICE to your
board and computer. Now you should be able to run `openocd`. It needs a config file, like the one
in this repo. (You may need to change the board.) You can pass this with the option `-f`, or just
run OpenOCD in the directory that contains the config file. If everything is working, OpenOCD will
say it is listening for connections.

Now in another terminal window (or tab or whatever) we can launch `gdb`. Run `gdb your_program.elf`.
To connect to OpenOCD, run `tar ext :3333` (really intuitive, right?). Now we're running GDB just
like before, but it's controlling your microcontroller. You may need to type `continue`, and it may
warn you that the program is already running. This is fine. To pause the program at any given
moment, hit `CTL-c`.
